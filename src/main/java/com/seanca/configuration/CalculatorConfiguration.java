package com.seanca.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.seanca.service.impl.Addition;
import com.seanca.service.impl.CalculatorServiceV2Impl;
import com.seanca.service.impl.Division;
import com.seanca.service.impl.Multiplication;
import com.seanca.service.impl.Subtract;

@Configuration
public class CalculatorConfiguration {
	

	@Bean
	CalculatorServiceV2Impl calculatorServiceV2Impl(Addition addition, Subtract subtract, Multiplication multiplication,
			Division division){
		return new CalculatorServiceV2Impl(addition, subtract, multiplication, division);
	}
	

}
