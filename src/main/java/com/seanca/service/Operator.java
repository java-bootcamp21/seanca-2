package com.seanca.service;

public interface Operator {
	
	Double apply(Double...digits);
	boolean supportOperator(String operatorType);

}
