package com.seanca.service.impl;

import java.util.List;

import org.springframework.stereotype.Component;
import com.seanca.service.Operator;

@Component
public class CalculatorServiceV1Impl{

	private final List<Operator> operators;
	
	public CalculatorServiceV1Impl(List<Operator> operators) {
		this.operators = operators;
	}

	public Double calculate(String operatorType,Double... digits) {
		return operators.stream()
				.filter(s -> s.supportOperator(operatorType))
				.findFirst()
				.map(s -> s.apply(digits))
				.orElseThrow(()-> new RuntimeException(String
						.format("Calculation error")));
	}
	
//	public Double calculateImperative(String operatorType,Double... digits){
//		Double result = null;
//		for(Operator operator : operators) {
//			if(operator.supportOperator(operatorType)) {
//				result = operator.apply(digits);
//				break;
//			}
//		}
//		if(result!=null) {
//			return result;
//		}else {
//			throw new RuntimeException(String.format("Calculation error"));
//		}
//	}

}
