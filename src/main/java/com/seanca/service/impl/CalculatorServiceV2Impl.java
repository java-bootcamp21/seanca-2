package com.seanca.service.impl;

import com.seanca.model.OperatorEnum;

public class CalculatorServiceV2Impl{

	private final Addition addition;
	private final Subtract subtract;
	private final Multiplication multiplication;
	private final Division division;
	

	public CalculatorServiceV2Impl(Addition addition, Subtract subtract, Multiplication multiplication,
			Division division) {
		this.addition = addition;
		this.subtract = subtract;
		this.multiplication = multiplication;
		this.division = division;
	}



	public Double calculate(String operatorType,Double... digits) {
		return switch(OperatorEnum.fromValue(operatorType)) {
		case ADDITION -> addition.apply(digits);
		case SUBTRACT -> subtract.apply(digits);
		case DIVISION -> division.apply(digits);
		case MULTIPLY -> multiplication.apply(digits);
		default -> throw new RuntimeException();
		};
	}



	

}
