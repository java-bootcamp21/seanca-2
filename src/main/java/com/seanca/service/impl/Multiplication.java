package com.seanca.service.impl;

import org.springframework.stereotype.Service;

import com.seanca.model.OperatorEnum;
import com.seanca.service.Operator;

@Service
public class Multiplication implements Operator{
	
	private OperatorEnum operator = OperatorEnum.MULTIPLY;

	@Override
	public Double apply(Double... digits) {
		return digits[0]*digits[1];
	}

	@Override
	public boolean supportOperator(String operatorType) {
		return operator == OperatorEnum.fromValue(operatorType);
	}
}