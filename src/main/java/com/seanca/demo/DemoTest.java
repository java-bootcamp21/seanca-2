package com.seanca.demo;

public class DemoTest {
	
	private AlphabetLetter letter;

	public DemoTest(AlphabetLetter letter) {
		this.letter = letter;
	}
	
	public String getInfo(){
		return letter.getInfo();
	}

}
