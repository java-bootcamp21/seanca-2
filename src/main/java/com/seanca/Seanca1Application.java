package com.seanca;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.seanca.service.Operator;
import com.seanca.service.impl.CalculatorServiceV1Impl;
import com.seanca.service.impl.CalculatorServiceV2Impl;

import static com.seanca.model.OperatorEnum.*;



@SpringBootApplication
public class Seanca1Application implements CommandLineRunner{
	
	private static final Logger logger = LoggerFactory.getLogger(Seanca1Application.class);

	private final CalculatorServiceV2Impl calculatorServiceV2Impl;
	
	private final CalculatorServiceV1Impl calculatorServiceV1Impl;

	
	
	public Seanca1Application(CalculatorServiceV2Impl calculatorServiceV2Impl,
			CalculatorServiceV1Impl calculatorServiceV1Impl) {
		this.calculatorServiceV2Impl = calculatorServiceV2Impl;
		this.calculatorServiceV1Impl = calculatorServiceV1Impl;
	}

	public static void main(String[] args) {
		SpringApplication.run(Seanca1Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		logger.info("calculatorServiceV1Impl - Addition {}",calculatorServiceV1Impl.calculate(ADDITION.getValue(), 5.0,5.0));	
		logger.info("calculatorServiceV1Impl - Subtraction {}",calculatorServiceV1Impl.calculate(SUBTRACT.getValue(), 5.0,5.0));	
		logger.info("calculatorServiceV1Impl - Multiplication {}",calculatorServiceV1Impl.calculate(MULTIPLY.getValue(), 5.0,5.0));	
		logger.info("calculatorServiceV1Impl - Division {}",calculatorServiceV1Impl.calculate(DIVISION.getValue(), 5.0,5.0));	

		logger.info("calculatorServiceV2Impl - Adition {}",calculatorServiceV2Impl.calculate(ADDITION.getValue(), 5.0,5.0));	
		logger.info("calculatorServiceV2Impl - Substraction {}",calculatorServiceV2Impl.calculate(SUBTRACT.getValue(), 5.0,5.0));	
		logger.info("calculatorServiceV2Impl - Multiplication {}",calculatorServiceV2Impl.calculate(MULTIPLY.getValue(), 5.0,5.0));	
		logger.info("calculatorServiceV2Impl - Division {}",calculatorServiceV2Impl.calculate(DIVISION.getValue(), 5.0,5.0));	

	}

}
