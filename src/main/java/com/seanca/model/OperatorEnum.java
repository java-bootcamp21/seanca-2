package com.seanca.model;

import java.util.Arrays;

public enum OperatorEnum {
	
	ADDITION("+"), SUBTRACT("-"), MULTIPLY("*"), DIVISION("/");
	
	private String value;

	private OperatorEnum(String value) {
		this.value = value;
	}
	
	public static OperatorEnum fromValue(String operatorType) {
		return Arrays.asList(OperatorEnum.values())
				.stream()
				.filter(e -> e.value.equals(operatorType))
				.findFirst()
				.orElseThrow(()-> new RuntimeException(String
						.format("Operator %s not found",operatorType)));
	
	}

	public String getValue() {
		return value;
	}
	

//	public static OperatorEnum fromValue1(String operatorType) {
//		List<OperatorEnum> enums = Arrays.asList(OperatorEnum.values());
//		OperatorEnum toReturnEnum = null;
//		for(OperatorEnum e : enums) {
//			if(e.value.equals(operatorType)) {
//				toReturnEnum = e;
//				break;
//			}
//		}
//		if(toReturnEnum==null) {
//			new RuntimeException(String
//					.format("Operator %s not found",operatorType));
//		}
//		return null;
//	
//	}

}
